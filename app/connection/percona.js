const Sequelize = require('sequelize');
const sequelize = new Sequelize(process.env.PERCONA_DB_NAME, process.env.PERCONA_USER, process.env.PERCONA_PASS, {
  host: process.env.PERCONA_HOST,
  dialect: 'mysql',
  operatorsAliases: false,
  pool: {
    max: 5,
    min: 0,
    acquire: 30000,
    idle: 10000
  }
});

module.exports = sequelize; 