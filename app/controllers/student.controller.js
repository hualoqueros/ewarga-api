const Student = require('../models/students.model.js');
const PendaftarSmp = require('../models/pendaftar_smp.model.js');
const PendaftarSmpTemp = require('../models/pendaftar_smp_temp.model.js'); 
const PendaftarSd = require('../models/pendaftar_sd.model.js');
const PilihanSmp = require('../models/pilihan_smp.model.js');
const PilihanSd = require('../models/pilihan_sd.model.js');
const MasterSd = require('../models/master_sd.model.js');
const Sekolah = require('../models/sekolah.model.js');
const District = require('../models/district.model.js');
const RegisteredStudent = require('../models/registered_student.model.js');
const sequelize = require('../connection/percona.js');
const utils = require('../helper/utils.js');
const Sequelize = require('sequelize');
var bcrypt = require('bcryptjs');
var randomstring = require("randomstring");
const Op = Sequelize.Op;


exports.findPendaftarSdByNik= (req, res) => {
    var nik = req.params.nik;
    PendaftarSd.findOne({
        where:{
            nik: nik
        },
        include: [{
            model: District,
            as: 'kelurahan'
        }]
    }).then(pendaftar => {
        if(!pendaftar){
            return res.status(404).send({
                message: "Theres no Pendaftar"
            });
        }
        
        District.findOne({
            where: {
                kode_wilayah: pendaftar.dataValues.kode_wilayah.slice(0,6)
            }
        }).then(kecamatan => {
            pendaftar.dataValues.kecamatan = kecamatan;
            res.send(pendaftar);
        })
        .catch(err => {
            res.status(500).send({
                message: err.message || "Some error occurred while finding the Pendaftar."
            });
        });
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred while finding the Pendaftar."
        });
    });
};

exports.findPendaftarSmpByNisn= (req, res) => {
    var nisn = req.params.nisn;
    PendaftarSmp.findOne({
        where:{
            nisn: nisn
        },
        include: [{
            model: District,
            as: 'kelurahan'
        }]
    }).then(pendaftar => {
        if(!pendaftar){
            return res.status(404).send({
                message: "Theres no Pendaftar"
            });
        }
        District.findOne({
            where: {
                kode_wilayah: pendaftar.dataValues.kode_wilayah.slice(0,6)
            }
        }).then(kecamatan => {
            pendaftar.dataValues.kecamatan = kecamatan;
            res.send(pendaftar);
        })
        .catch(err => {
            res.status(500).send({
                message: err.message || "Some error occurred while finding the Pendaftar."
            });
        });
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred while finding the Pendaftar."
        });
    });
};

exports.findPendaftarSdByNoDaftar = (req, res) => {
    var noDaftar = req.params.no_daftar;
    // Validate request
    if(!noDaftar) {
        return res.status(400).send({
            message: "No Daftar can not be empty"
        });
    }

    PendaftarSd.findOne({
        where: {
            no_daftar: noDaftar
        },
        include: [{
            model: District,
            as: 'kelurahan'
        }]
    }).then(pendaftar => {
        if(!pendaftar){
            return res.status(404).send({
                message: "Theres no pendaftar"
            });
        }

        District.findOne({
            where: {
                kode_wilayah: pendaftar.dataValues.kode_wilayah.slice(0,6)
            }
        }).then(kecamatan => {
            pendaftar.dataValues.kecamatan = kecamatan;

            PilihanSd.findAll({
                where:{
                    no_daftar: noDaftar
                },
                include: [{
                    model: Sekolah,
                    as: 'sekolah'
                }]
            }).then(pilihan => {
                if(!pilihan){
                    return res.status(404).send({
                        message: "Theres no pilihan"
                    });
                }
                pendaftar.dataValues.pilihan = pilihan;
                res.send(pendaftar);
            }).catch(err => {
                res.status(500).send({
                    message: err.message || "Some error occurred while finding the Pendaftar."
                });
            });
        })

        
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred while finding the Pendaftar."
        });
    });
};

exports.findPendaftarSmpByNoDaftar = (req, res) => {
    var noDaftar = req.params.no_daftar;
    // Validate request
    if(!noDaftar) {
        return res.status(400).send({
            message: "No Daftar can not be empty"
        });
    }

    PendaftarSmp.findOne({
        where: {
            no_daftar: noDaftar
        },
        include: [{
            model: District,
            as: 'kelurahan'
        }]
    }).then(pendaftar => {
        if(!pendaftar){
            return res.status(404).send({
                message: "Theres no pendaftar"
            });
        }

        District.findOne({
            where: {
                kode_wilayah: pendaftar.dataValues.kode_wilayah.slice(0,6)
            }
        }).then(kecamatan => {
            pendaftar.dataValues.kecamatan = kecamatan;

            PilihanSmp.findAll({
                where:{
                    no_daftar: noDaftar
                },
                include: [{
                    model: Sekolah,
                    as: 'sekolah'
                }]
            }).then(pilihan => {
                if(!pilihan){
                    return res.status(404).send({
                        message: "Theres no pilihan"
                    });
                }
                pendaftar.dataValues.pilihan = pilihan;
                res.send(pendaftar);
            }).catch(err => {
                res.status(500).send({
                    message: err.message || "Some error occurred while finding the Pendaftar."
                });
            });
        })

        
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred while finding the Pendaftar."
        });
    });
};

exports.findNikfromMaster = (req, res) => {
    var nik = req.params.nik;
    // Validate request
    if(!nik) {
        return res.status(400).send({
            message: "NIK can not be empty"
        });
    }

    MasterSd.findOne({
        where: {
            nik: nik
        }
    }).then(sd => {
        if(!sd){
            res.status(404).send({
                message: "Siswa tidak ditemukan"
            });
        }
        res.send(sd);
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred while finding the Pendaftar."
        });
    });
}

exports.findNisnfromMaster = (req, res) => {
    var nisn = req.params.nisn;
    // Validate request
    if(!nisn) {
        return res.status(400).send({
            message: "NISN can not be empty"
        });
    }

    Student.findOne({
        where: {
            nisn: nisn
        }
    }).then(smp => {
        if(!smp){
            res.status(404).send({
                message: "Siswa tidak ditemukan"
            });
        }
        res.send(smp);
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred while finding the Pendaftar."
        });
    });
}

/**
 * Student Listing
 * @param {*} req 
 * @param {*} res 
 */
exports.findAllStudent = (req, res) => {
    var limit   = parseInt(req.query.limit,20);
    var page    = parseInt(req.query.page,0);
    var start   = parseInt(page*limit);
    Student.findAndCountAll({
        limit: limit,
        offset: start
    }).then(student => {
        if(!student){
            return res.status(404).send({
                message: "Theres no student"
            });
        }
        
        res.send({
            draw: 1,
            recordsTotal: student.count,
            recordsFiltered: student.count,
            data: student.rows
        });
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred while finding the Pendaftar."
        });
    });
};

/**
 * Pendaftar SMP listing
 * @param {*} req 
 * @param {*} res 
 */
exports.findAllPendaftarSmp= (req, res) => {
    var limit   = parseInt(req.query.limit,20);
    var page    = parseInt(req.query.page,0);
    var start   = parseInt(page*limit);
    PendaftarSmp.findAndCountAll({
        limit: limit,
        offset: start
    }).then(pendaftar => {
        if(!pendaftar){
            return res.status(404).send({
                message: "Theres no Pendaftar"
            });
        }
        
        res.send({
            draw: 1,
            recordsTotal: pendaftar.count,
            recordsFiltered: pendaftar.count,
            data: pendaftar.rows
        });
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred while finding the Pendaftar."
        });
    });
};

/**
 * Pendaftar SMP listing
 * @param {*} req 
 * @param {*} res 
 */
exports.findAllPendaftarSd= (req, res) => {
    var limit   = parseInt(req.query.limit,20);
    var page    = parseInt(req.query.page,0);
    var start   = parseInt(page*limit);
    PendaftarSd.findAndCountAll({
        limit: limit,
        offset: start
    }).then(pendaftar => {
        if(!pendaftar){
            return res.status(404).send({
                message: "Theres no Pendaftar"
            });
        }
        
        res.send({
            draw: 1,
            recordsTotal: pendaftar.count,
            recordsFiltered: pendaftar.count,
            data: pendaftar.rows
        });
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred while finding the Pendaftar."
        });
    });
};



exports.registerStudentSmp = (req, res) => {

    var no_ujian            = req.body.no_ujian || null;
    var nisn                = req.body.nisn || null;
    var nama                = req.body.nama || null;
    var jenis_kelamin       = req.body.jenis_kelamin || null;
    var tempat_lahir        = req.body.tempat_lahir || null;
    var tanggal_lahir       = req.body.tanggal_lahir || null;
    var alamat              = req.body.alamat || null;
    var kota                = req.body.kota || null;
    var no_tlp              = req.body.no_tlp || null;
    var nama_ortu           = req.body.nama_ortu || null;
    var asal_sekolah        = req.body.asal_sekolah || null;
    var kota_asal_sekolah   = req.body.kota_asal_sekolah || null;
    var un_mat              = req.body.un_mat || 0;
    var un_ipa              = req.body.un_ipa || 0;
    var un_bind             = req.body.un_bind || 0;
    var us_mat              = 0;
    var us_ipa              = 0;
    var us_bin              = 0;
    var nil_prestasi        = req.body.nil_prestasi || 0;
    var nil_usia            = req.body.nil_usia || 0;
    var nil_khusus          = req.body.nil_khusus || 0;
    var is_dalam_kota       = req.body.is_dalam_kota || null;
    var waktu_daftar        = req.body.waktu_daftar || null;
    var operator_id         = req.body.operator_id || null;
    var ip_address          = req.body.ip_address || null;
    var k_lokasi            = null;
    var k_terima            = null;
    var pil_terima          = null;
    var tahap               = req.body.tahap || null;
    var urutan              = req.body.urutan || null;
    var kode_wilayah        = req.body.kode_wilayah || null;
    var ket_hapus           = null;
    var is_daftar_ulang     = null;
    var tgl_daftar_ulang    = null;
    var operator_approve_daftar_ulang   = req.body.operator_approve_daftar_ulang || null;
    var tgl_approve_daftar_ulang        = req.body.tgl_approve_daftar_ulang || null;
    var approve_daftar_online           = req.body.approve_daftar_online || null;
    var tgl_approve_daftar_online       = req.body.tgl_approve_daftar_online || null;
    var operator_approve_daftar_online  = req.body.operator_approve_daftar_online || null;
    var pilihan = req.body.pilihan;
    var jalur_id = req.body.jalur_id;

    var onDate = process.env.ON_DATE_SMP;
    console.log(utils);
    var age = utils.calcAge(tanggal_lahir,onDate);
    console.log(age);
    if (age > parseInt(process.env.AGE_MAX_SMP)){
        res.status(400).send({
            message: "Usia melebihi dari 15 Tahun"
        });
        return ;
    } 

    console.log("--Trying to Save data--");
    // Create a User
    // find no_daftar is exist or not
    const savingData = async () => {
        try {
            let notAvailable = true;
            var generateNoDaftar = "";
            
            
            while(notAvailable){
                var rand = Math.floor(Math.random() * 1000) + 1;
                generateNoDaftar = "18" + pilihan[0] + utils.zeroLeading( rand, 4);
                console.log("##first##" + generateNoDaftar);
                
                await PendaftarSmp.findAll({
                    where: {
                        no_daftar: generateNoDaftar  
                    }
                }).then(finded => {
                    console.log(finded);
                    if(finded.length <=0 ){
                        notAvailable = false;
                    }
                }).catch(err => {
                    throw err;
                });
            }

            console.log("##final no_daftar##" + generateNoDaftar);

            var resultData =  await sequelize.transaction(function(t){
                var nilai_asal_sekolah = 10;
                if (kota_asal_sekolah=="KOTA GORONTALO"){
                    nilai_asal_sekolah = 50;
                }
                console.log("aslah sekolah",kota_asal_sekolah,nilai_asal_sekolah);
                return PendaftarSmp.create({
                    no_daftar: generateNoDaftar,
                    no_ujian: no_ujian, 
                    nisn: nisn,
                    nama: nama,
                    jenis_kelamin: jenis_kelamin,
                    tempat_lahir: tempat_lahir,
                    tanggal_lahir: tanggal_lahir,
                    alamat: alamat,
                    kota: kota,
                    kode_wilayah: kode_wilayah,
                    no_tlp: no_tlp,
                    nama_ortu: nama_ortu,
                    asal_sekolah: asal_sekolah,
                    kota_asal_sekolah: kota_asal_sekolah,
                    un_mat: un_mat,
                    un_ipa: un_ipa,
                    un_bind: un_bind,
                    us_mat: us_mat,
                    us_ipa: us_ipa,
                    us_bin: us_bin,
                    nil_prestasi: nil_prestasi,
                    nil_usia: nil_usia,
                    nil_khusus: nil_khusus,
                    nil_asal_sekolah: nilai_asal_sekolah,
                    is_dalam_kota: is_dalam_kota,
                    waktu_daftar: waktu_daftar,
                    operator_id: operator_id,
                    ip_address: ip_address,
                    tahap: tahap,
                    urutan: urutan,
                    tanggal_lahir: tanggal_lahir,
                },{
                    transaction:t
                }).then(data => {
                    var listPilihan = [];
                    return Sekolah.findAll({
                                where: {
                                    id:{
                                        [Op.in]:pilihan
                                    }
                                }
                            }).then(schools => {
                                console.log("sekolah list",schools);

                                schools.forEach((school, key) => {
                                    pilihan.forEach((item, index) => {
                                        if(item==school.id){
                                            var nilai_ppdb = utils.calcPPDB(data.dataValues, jalur_id, school.kode_wilayah);
                                            console.log("NILAI PPDB");
                                            console.log(nilai_ppdb);
                                            listPilihan.push({
                                                no_daftar: generateNoDaftar,
                                                pilih_urut: index+1,
                                                sekolah_id: item,
                                                jalur_id: jalur_id,
                                                nilai_zona: nilai_ppdb.nilaiZona,
                                                nilai_ppdb: nilai_ppdb.nilaiPPDB
                                            });
                                        }
                                    });
                                });
                                console.log("listPilihan",listPilihan);

                                return PilihanSmp.bulkCreate(listPilihan,{
                                    transaction: t
                                }).then(() => {
                                    console.log("pilihan saved");
                                    data.dataValues.pilihan = listPilihan;
                                    return data;
                                });

                            });
                }).catch(err => {
                    throw new Error(err.errors[0].message);
                });
                
            }).then(result =>{
                console.log("success");
                console.log(result);
                
                res.send(result);
            }).catch(err => {
                console.log("masuk err transaction");
                console.log(err.message);
                throw err;
            });
        
            
        }catch(err){
            res.status(500).send({
                message: err.message || "Some error occurred while creating the Pendaftar."
            });
        }
    }
    savingData();
    console.log("##response##");
    
} 

exports.registerStudentSd = (req, res) => {
    var no_akte             = req.body.no_akte || null;
    var nik                 = req.body.nik;
    var nama                = req.body.nama || null;
    var jenis_kelamin       = req.body.jenis_kelamin || null;
    var tempat_lahir        = req.body.tempat_lahir || null;
    var tanggal_lahir       = req.body.tanggal_lahir || null;
    var alamat              = req.body.alamat || null;
    var kota                = req.body.kota || null;
    var no_tlp              = req.body.no_tlp || null;
    var nama_ortu           = req.body.nama_ortu || null;
    var asal_sekolah        = req.body.asal_sekolah || null;
    var kota_asal_sekolah   = req.body.kota_asal_sekolah || null;
    var is_dalam_kota       = req.body.is_dalam_kota || null;
    var waktu_daftar        = req.body.waktu_daftar || null;
    var operator_id         = req.body.operator_id || null;
    var ip_address          = req.body.ip_address || null;
    var tahap               = req.body.tahap || null;
    var urutan              = req.body.urutan || null;
    var kode_wilayah        = req.body.kode_wilayah || null;

    var pilihan = req.body.pilihan;
    var jalur_id = req.body.jalur_id;
    var nil_khusus = req.body.nil_khusus || 0;

    var onDate = process.env.ON_DATE_SD;
    console.log(utils);
    var age = utils.parseAge(tanggal_lahir,onDate);
    console.log("cek umur",age,process.env.AGE_YEAR_MIN_SD);
    if(age.years < process.env.AGE_YEAR_MIN_SD){
        res.status(400).send({
            message: "Usia siswa masih "+age.text
        });
    }
    
    // finding nilai usia
    var nilai_usia = 0;
    if (age.years >= parseInt(process.env.AGE_MAX_SD)){
        nilai_usia = 120;
    }else if(age.years == parseInt(process.env.AGE_YEAR_MIN_SD) && age.months == parseInt(process.env.AGE_MONTH_MIN_SD) ){
        nilai_usia = 70;
    }else if(age.years > parseInt(process.env.AGE_YEAR_MIN_SD) && age.months > parseInt(process.env.AGE_MONTH_MIN_SD) ){
        if(age.years <= parseInt(process.env.AGE_MAX_YEAR_PHASE2_SD) && age.months <= parseInt(process.env.AGE_MAX_MONTH_PHASE2_SD)){
          nilai_usia = 80;
        }
    }else if(age.years <= parseInt(process.env.AGE_YEAR_MIN_SD) && age.months < parseInt(process.env.AGE_MONTH_MIN_SD)){
        return res.status(400).send({
            message: age.text
        });
    }

    console.log("--Trying to Save data--");
    // Create a User
    // find no_daftar is exist or not
    const savingData = async () => {
        try{
            let notAvailable = true;
            var generateNoDaftar = "";
            while(notAvailable){
                var rand = Math.floor(Math.random() * 1000) + 1;
                generateNoDaftar = "18" + pilihan[0] + utils.zeroLeading( rand, 4);
                console.log("##first##" + generateNoDaftar);
                
                await PendaftarSd.findAll({
                    where: {
                        no_daftar: generateNoDaftar  
                    }
                }).then(finded => {
                    console.log(finded);
                    if(finded.length <=0 ){
                        notAvailable = false;
                    }
                }).catch(err => {
                    res.status(500).send({
                        message: err.message || "Some error occurred while creating the Pendaftar."
                    });
                });
            }


            console.log("##final no_daftar##" + generateNoDaftar);

            var resultData =  await sequelize.transaction(function(t){
                return PendaftarSd.create({
                    no_daftar: generateNoDaftar,
                    nik: nik,
                    no_akte: no_akte, 
                    nama: nama,
                    jenis_kelamin: jenis_kelamin,
                    tempat_lahir: tempat_lahir,
                    tanggal_lahir: tanggal_lahir,
                    alamat: alamat,
                    kota: kota,
                    kode_wilayah: kode_wilayah,
                    no_tlp: no_tlp,
                    nama_ortu: nama_ortu,
                    nil_usia: nilai_usia,
                    nil_khusus: nil_khusus,
                    asal_sekolah: asal_sekolah,
                    kota_asal_sekolah: kota_asal_sekolah,
                    is_dalam_kota: is_dalam_kota,
                    waktu_daftar: waktu_daftar,
                    operator_id: operator_id,
                    ip_address: ip_address,
                    tahap: tahap,
                    urutan: urutan,
                    tanggal_lahir: tanggal_lahir
                },{
                    transaction:t
                }).then(data => {
                    var listPilihan = [];
                    return Sekolah.findAll({
                                where: {
                                    id:{
                                        [Op.in]:pilihan
                                    }
                                }
                            }).then(schools => {
                                console.log("sekolah list",schools);

                                schools.forEach((school, key) => {
                                    pilihan.forEach((item, index) => {
                                        if(item==school.id){
                                            var nilai_ppdb = utils.calcPPDBSd(data.dataValues, jalur_id, school.kode_wilayah);
                                            console.log("NILAI PPDB");
                                            console.log(nilai_ppdb);
                                            listPilihan.push({
                                                no_daftar: generateNoDaftar,
                                                pilih_urut: index+1,
                                                sekolah_id: item,
                                                jalur_id: jalur_id,
                                                nilai_zona: nilai_ppdb.nilaiZona,
                                                nilai_ppdb: nilai_ppdb.nilaiPPDB
                                            });
                                        }
                                    });
                                });
                                console.log("listPilihan",listPilihan);

                                return PilihanSd.bulkCreate(listPilihan,{
                                    transaction:t
                                }).then(() => {
                                    console.log("pilihan saved");
                                    data.dataValues.pilihan = listPilihan;
                                    return data;
                                });

                            })

                }).catch(err => {
                    throw new Error(err.errors[0].message);
                });
                
            }).then(result =>{
                console.log("success");
                console.log(result);
                
                res.send(result);
            }).catch(err => {
                console.log("masuk err transaction");
                console.log(err.message);
                throw err;
            });
        }catch(err){
            res.status(500).send({
                message: err.message || "Some error occurred while creating the Pendaftar."
            });
        }
    }
    savingData();
    console.log("##response##");
            
} 

exports.regisOnlineSd = (req, res) => {
    
    var username        = req.body.username || null;
    var email           = req.body.email || null;
    // var tanggal_lahir   = req.body.tanggal_lahir || null;
    var jenjang_id      = req.body.jenjang_id || null;

    const processRegis = async () => {
        try{
            var findSiswa = await MasterSd.findOne({
                    where:{
                        nik: username
                    }
                }).then(result => {
                    if(!result){
                        throw new Error("Siswa tidak ditemukan");
                    }
                    return result;
                }).catch(err => {
                    throw new Error(err.message);
                });
            
            // if (tanggal_lahir != findSiswa.tanggal_lahir){
            //     throw new Error("Tanggal lahir salah!");
            // }

            if (findSiswa){
                // check if user has registered
                await RegisteredStudent.findOne({
                    where:{
                        username: username
                    }
                }).then(finded => {
                    console.log(finded);
                    if(finded){
                        throw new Error("NIK sudah pernah terdaftar, silahkan gunakan lupa password.");
                    }
                });

                await RegisteredStudent.findOne({
                    where:{
                        email: email
                    }
                }).then(finded => {
                    if(finded){
                        throw new Error("Email sudah pernah terdaftar, silahkan gunakan lupa password.");
                    }
                });

                var realPass = randomstring.generate(8);
                var hash = bcrypt.hashSync(realPass, parseInt(process.env.SALT_ROUND));
                var resultData =  await sequelize.transaction(function(t){
                    return RegisteredStudent.create({
                            username: username,
                            jenjang: jenjang_id,
                            email: email,
                            password: hash
                        },{
                            transaction:t
                        }).then(result => {
                            return result;
                        }).catch(err => {
                            throw new Error(err.message);
                        });
                }); 
                resultData.dataValues.real_pass = realPass;
                res.send(resultData);
            }
        }catch(err){
            res.status(400).send(
                {
                    message: err.message
                }
            );
        }
    }

    processRegis();
}

exports.regisOnlineSmp = (req, res) => {
    
    var username        = req.body.username || null;
    var email           = req.body.email || null;
    // var tanggal_lahir   = req.body.tanggal_lahir || null;
    var jenjang_id      = req.body.jenjang_id || null;
    
    const processRegis = async () => {
        try{
            var findSiswa = await Student.findOne({
                    where:{
                        nisn: username
                    }
                }).then(result => {
                    if(!result){
                        throw new Error("Siswa tidak ditemukan");
                    }
                    return result;
                }).catch(err => {
                    throw new Error(err.message);
                });
            // console.log('lahir',tanggal_lahir,findSiswa.tanggal_lahir);
            // if (tanggal_lahir != findSiswa.tanggal_lahir){
            //     throw new Error("Tanggal lahir salah!");
            // }

            if (findSiswa){
                var realPass = randomstring.generate(8);
                var hash = bcrypt.hashSync(realPass, parseInt(process.env.SALT_ROUND));
                var resultData =  await sequelize.transaction(function(t){
                    return RegisteredStudent.create({
                            username: username,
                            jenjang: jenjang_id,
                            email: email,
                            password: hash
                        },{
                            transaction:t
                        }).then(result => {
                            return result;
                        }).catch(err => {
                            throw new Error(err.message);
                        });
                }); 
                resultData.dataValues.real_pass = realPass;
                res.send(resultData);
            }
        }catch(err){
            res.status(400).send(
                {
                    message: err.message
                }
            );
        }
    }

    processRegis();
}

exports.forgotPassword = (req, res) => {
    var email       = req.body.email;
    var username    = req.body.username;

    // finding registered user with email
    const processForgotPassword = async () => {
        try{
            var type = "NIK";
            var student = await RegisteredStudent.findOne({
                where: {
                    email: email,
                    username: username
                }
            }).then(result => {
                if(!result){
                    throw new Error("NIK/NISN atau Email belum terdaftar");
                }
                return result;
                
            }).catch(err => {
                throw new Error(err.message);
            });

            

            if (student){
                type = student.jenjang == 5 ? "NIK" : "NISN";
                var realPass = randomstring.generate(8);
                var hash = bcrypt.hashSync(realPass, parseInt(process.env.SALT_ROUND));
                var updateData =  await sequelize.transaction(function(t){
                                            return RegisteredStudent.update({
                                                    password: hash
                                                },{
                                                    where: {
                                                        email: email,
                                                        username: username,
                                                    },
                                                    transaction:t
                                                }).then(result =>{
                                                    console.log("update result",result);
                                                    return result;
                                                }).catch(err =>{
                                                    throw err;
                                                });
                                        });
                
                if (updateData>0){
                    // utils.sendMailGmail(student.email, student.username, realPass, type);
                    student.dataValues.real_pass = realPass;
                    res.send(student);

                }else{
                    throw new Error("Gagal reset password");
                }
            }else{
                throw new Error("Siswa belum terdaftar");
            }

        }catch(err){
            res.status(400).send({
                message: err.message
            });
        }
    }

    processForgotPassword();


}

exports.loginUserOnline = (req, res) => {
    var username    = req.body.username;
    var password    = req.body.password
    

    const login = async () => {
        try{
            var student = await RegisteredStudent.findOne({
                where: {
                    username: username
                }
            }).then(result => {
                return result;
            });

            if (student){
                var passed = await bcrypt.compare(password, student.password)
                                .then(result =>{
                                return result;
                            });

                console.log("passed: ",passed);
                if (passed){
                    return res.send(student);
                }else{
                    throw new Error("Password salah!");
                }
            }
            throw new Error("Data siswa tidak ditemukan");
        
        }catch(err){
            res.status(400).send(
                {
                    message: err.message
                }
            );
        }
    }

    login();
}

exports.findAllRegistered = (req, res) => {
    var limit   = parseInt(req.query.limit,20);
    var page    = parseInt(req.query.page,0);
    var start   = parseInt(page*limit);
    RegisteredStudent.findAndCountAll({
        limit: limit,
        offset: start,
    }).then(student => {
        if(!student){
            return res.status(404).send({
                message: "Theres no student"
            });
        }
        
        res.send({
            draw: 1,
            recordsTotal: student.count,
            recordsFiltered: student.count,
            data: student.rows
        });
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred while finding the Pendaftar."
        });
    });
}

exports.findRegisOnlineByUsername = (req, res) => {
    var username = req.params.username;
    var limit   = parseInt(req.query.limit,20);
    var page    = parseInt(req.query.page,0);
    var start   = parseInt(page*limit);
    RegisteredStudent.findOne({
        where: {
            username: username
        }
    }).then(student => {
        if(!student){
            return res.status(404).send({
                message: "Theres no student"
            });
        }
        
        res.send(student);
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred while finding the Pendaftar."
        });
    });
}