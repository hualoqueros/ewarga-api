const Surat = require('../models/surat.model.js');
const RequestSurat = require('../models/request_surat.model.js');
const RequestSuratRevision = require('../models/request_surat_revision.model.js');
const Sequelize = require('sequelize');
const sequelize = require('../connection/percona.js');
const Status = require('../models/status.model.js')
const User = require('../models/user.model.js');
var kodeMasaPengurusan = "215"
var kodeTahun = new Date()
var addZero = require('add-zero');


exports.getAll = (req, res) => {
    Surat.findAll().then(surat => {
        return res.send(surat)
    }).catch(err => {
        return res.status(400).send({
            message: err.message
        })
    })
}

exports.addRequest = (req, res) => {
    var suratId = req.body.surat_id
    var wargaId = req.body.warga_id
    var images = req.files

    console.log(images.images)
    
    const savingData = async () => {
        try{
            // get warga data
            var user = await User.findOne({
                where: {
                    id: wargaId
                }
            })
            .then(founded => { 
                if (founded!=null){
                    return founded
                }
                throw new Error("Data user tidak ditemukan") 
            })
            .catch(err => { throw err })
            

            var nextId = await sequelize.query("SHOW TABLE STATUS LIKE 'request_surat'")
                                        .spread((results, metadata) => {
                                            return results[0].Auto_increment
                                        })
            var kodeSurat = addZero(user.id, 6) + "/" + addZero(nextId,3) + "/" + kodeMasaPengurusan + "/" + "PRT.001/" + kodeTahun.getFullYear()    
            var resultData =  await sequelize.transaction(function(t){

                return RequestSurat.create({
                    surat_id: suratId,
                    warga_id: user.id,
                    no_surat: kodeSurat,
                    status: Status.REQUEST.DRAFT
                },{
                    transaction: t
                })
                .then( requestCreated => {
                    console.log("sukses ",requestCreated.dataValues)
                    var i = 0;
                    var upload = await images.images.forEach((image)=>{
                            
                            image.mv('/usr/src/images/surat/'+requestCreated.dataValues.no_surat+'-'+i+'.jpg', function(err) {
                                if (err)
                                    return res.status(500).send(err);
                            console.log("berhasil " + i )
                            }); 
                        i++;
                        })

                    var approval = [1,2]
                    var revision = []
                    approval.forEach(item => {
                        revision.push({
                            request_surat_id: requestCreated.dataValues.id,
                            warga_id: item,
                            status: Status.REVISION.DRAFT
                        })
                    });

                    return RequestSuratRevision.bulkCreate(revision,{
                        transaction: t
                    })
                    .then( rev => {
                        requestCreated.dataValues.revision = rev
                        return requestCreated.dataValues
                    })
                    .catch(err =>{
                        console.log("ERROR CREATE REQUEST SURAT REVISION " + err)
                        throw err
                    })

                    
                    
                })
                .catch(err => {
                    console.log("ERROR CREATE REQUEST SURAT " + err)
                    throw err
                })
            })
            .then(result => {
                var response = RequestSurat.findOne({
                    where: {
                        id: result.id
                    },
                    include: [{
                        model: Surat,
                        as: 'surat'
                    }]
                }).then(reqSurat => {
                    result.surat = reqSurat.surat
                    return res.send(result)
                })
            }).catch(err => {
                throw err
            })
        }catch(e){
            return res.status(400).send({
                message: e.message
            })
        }
    }

    savingData();
}

