var bcrypt = require('bcryptjs');
const sequelize = require('../connection/percona.js');
const User = require('../models/user.model.js');
const Status = require('../models/status.model.js')

exports.login = (req, res) => {
    const errors = validationLoginResult(req);
    if (!errors.isEmpty()) {
        return res.status(422).json({ errors: errors.array() });
    }

    var email = req.body.email
    var password = req.body.password

    // get user by email
    var login = async (password) => {
        try{
            var user = await User.findOne({
                where: {
                    email: email
                }
            })
            .then(founded => { 
                if(founded!=null){
                    return founded
                }
                throw new Error("user tidak ditemukan") 
            })
            .catch(err => { throw err })
            console.log("DEBUG")
            console.log(password) 
            console.log(user) 
            var verified = bcrypt.compareSync(password,user.password)
            if (verified){
                return res.send(user)
            }else{
                return res.status(403).send({
                    message: "Login gagal"
                })
            }
        }catch(err){
            res.status(403).send({
                message: err.message
            })
        }
    }
    login(password)
}

exports.register = (req, res) => {
    const errors = validationRegisterResult(req);
    if (!errors.isEmpty()) {
        return res.status(422).json({ errors: errors.array() });
    }

    var email       = req.body.email
    var firstname   = req.body.first_name
    var lastname    = req.body.last_name || ""
    var password    = req.body.password
    var rw          = req.body.rw || 1
    var rt          = req.body.rt || 1
    var nik         = req.body.nik

    var salt = bcrypt.genSaltSync(10);
    var hash = bcrypt.hashSync(password, salt);

    var saving =  sequelize.transaction(function(t){
        return User.create({
                    type: 1,
                    email: email,
                    nik: nik,
                    firstname: firstname,
                    lastname: lastname,
                    rw: rw,
                    rt: rt,
                    password: hash,
                    status: Status.USER.ACTIVE
                },{ 
                    transaction: t
                })
                .then(userCreated => {
                    return res.status(201).send(userCreated.dataValues)
                })
                .catch(err => { 
                    return res.status(400).send(
                        {
                            message: err.message
                        }
                    )
                })
    })

}