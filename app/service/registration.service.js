const Student = require('../models/students.model.js');
const PendaftarSmp = require('../models/pendaftar_smp.model.js');
const PendaftarSmpTemp = require('../models/pendaftar_smp_temp.model.js'); 
const PendaftarSd = require('../models/pendaftar_sd.model.js');
const PilihanSmp = require('../models/pilihan_smp.model.js');
const PilihanSd = require('../models/pilihan_sd.model.js');
const MasterSd = require('../models/master_sd.model.js');
const Sekolah = require('../models/sekolah.model.js');
const District = require('../models/district.model.js');
const RegisteredStudent = require('../models/registered_student.model.js');
const sequelize = require('../connection/percona.js');
const utils = require('../helper/utils.js');
const Sequelize = require('sequelize');
var bcrypt = require('bcryptjs');
var randomstring = require("randomstring");
const Op = Sequelize.Op;

module.exports = {
    generateNoDaftar: function(pilihan) {
       
        let notAvailable = true;
        var generateNoDaftar = "";
        while(notAvailable){
            var rand = Math.floor(Math.random() * 1000) + 1;
            generateNoDaftar = "18" + pilihan[0] + utils.zeroLeading( rand, 4);
            console.log("##first##" + generateNoDaftar);
            
            PendaftarSd.findAll({
                where: {
                    no_daftar: generateNoDaftar  
                }
            }).then(finded => {
                console.log("finded",finded);
                if(finded.length <=0 ){
                    notAvailable = false;
                }
                return generateNoDaftar;
            });
        }
    }
}