module.exports = (app) => {
    const student = require('../controllers/student.controller.js');

    app.get('/master/sd/:nik', student.findNikfromMaster);
    app.get('/master/smp/:nisn', student.findNisnfromMaster);

    app.get('/pendaftar/sd', student.findAllPendaftarSd);
    app.get('/pendaftar/smp', student.findAllPendaftarSmp);

    app.get('/pendaftar/sd/nik/:nik', student.findPendaftarSdByNik);
    app.get('/pendaftar/smp/nisn/:nisn', student.findPendaftarSmpByNisn);
    // app.get('/students/:nisn', student.findByNisn);
    app.get('/pendaftar', student.findAllStudent);

    // app.post('/pendaftar/smp/:no_daftar/approve', student.approveSmp);
    app.post('/pendaftar/smp', student.registerStudentSmp);
    // app.post('/pendaftar/sd/:no_daftar/approve', student.approveSd);
    app.post('/pendaftar/sd', student.registerStudentSd);

    app.get('/pendaftar/sd/:no_daftar', student.findPendaftarSdByNoDaftar);
    app.get('/pendaftar/smp/:no_daftar', student.findPendaftarSmpByNoDaftar);

    
    app.post('/daftar/sd', student.regisOnlineSd); 
    app.post('/daftar/smp', student.regisOnlineSmp);
    
    app.post('/login', student.loginUserOnline);

    app.post('/forgot-password', student.forgotPassword);

    app.get('/pendaftar/all', student.findAllRegistered);
    app.get('/pendaftar/:username', student.findRegisOnlineByUsername);


}