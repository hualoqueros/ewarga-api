const { body, validationResult } = require('express-validator/check');

const middleware = [ body('email')
                        .not().isEmpty().withMessage('email kosong'),
                     body('email')
                        .isEmail().withMessage('format email salah'),
                     body('password','masukkan password').not().isEmpty() ]
global.validationLoginResult = validationResult
module.exports = middleware;