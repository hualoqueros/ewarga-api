const { body, validationResult } = require('express-validator/check');

const middleware = [ body('email')
                        .not().isEmpty().withMessage('email kosong'),
                     body('email')
                        .isEmail().withMessage('format email salah'),
                     body('password','masukkan password').not().isEmpty(),
                     body('first_name','masukkan nama depan anda').not().isEmpty(),
                     body('nik','masukkan NIK anda').not().isEmpty() ]
global.validationRegisterResult = validationResult
module.exports = middleware;