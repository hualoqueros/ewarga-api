module.exports = (app) => {
    const user = require('../controllers/user.controller.js')
    const registerMiddleware = require('./middleware/register.middleware.js')
    const loginMiddleware = require('./middleware/login.middleware.js')
    app.post('/login', loginMiddleware, user.login)
    app.post('/register', registerMiddleware, user.register)
}