const Sequelize = require('sequelize');
const sequelize = require('../connection/percona.js');
const user = sequelize.define('warga', {
    id: {
      type: Sequelize.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    type: {
      type: Sequelize.INTEGER
    },
    nik: {
      type: Sequelize.STRING,
      unique: true
    },
    firstname: {
        type: Sequelize.STRING
    },
    lastname: {
        type: Sequelize.STRING
    },
    email: {
        type: Sequelize.STRING,
        unique: true
    },
    password: {
        type: Sequelize.STRING
    },
    provinsi: {
        type: Sequelize.INTEGER
    },
    kabupaten: {
        type: Sequelize.INTEGER
    },
    kelurahan: {
        type: Sequelize.INTEGER
    },
    rw: {
        type: Sequelize.INTEGER
    },
    rt: {
        type: Sequelize.INTEGER
    },
    status: {
      type: Sequelize.INTEGER
    }
  },
  {
    freezeTableName: true,
    underscored : true,
    timestamps: true
  });

  module.exports = user;