let STATUS = {
    REQUEST : {
        DRAFT: 1,
        PROGRESS: 2,
        FINISH: 3,
    },
    REVISION : {
        DRAFT: 1,
        APPROVED: 2,
        REJECTED: 3
    },
    USER : {
        ACTIVE: 1,
        INACTIVE: 2,
        DELETED: 3
    }
}

module.exports = STATUS;