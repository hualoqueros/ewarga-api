const Sequelize = require('sequelize');
const sequelize = require('../connection/percona.js');
const RequestSuratRevision = sequelize.define('request_surat_revision', {
    id: {
      type: Sequelize.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    request_surat_id: {
      type: Sequelize.INTEGER
    },
    warga_id: {
      type: Sequelize.INTEGER
    },
    status: {
      type: Sequelize.INTEGER
    }
  },
  {
    freezeTableName: true,
    underscored : true,
    timestamps: true
  });

  module.exports = RequestSuratRevision;