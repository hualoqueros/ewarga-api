const Sequelize = require('sequelize');
const sequelize = require('../connection/percona.js');
const surat = sequelize.define('surat', {
    id: {
      type: Sequelize.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    nama: {
      type: Sequelize.STRING
    },
    status: {
      type: Sequelize.INTEGER
    }
  },
  {
    freezeTableName: true,
    underscored : true,
    timestamps: true
  });

  module.exports = surat;