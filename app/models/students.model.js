const Sequelize = require('sequelize');
const sequelize = require('../connection/percona.js');
const pendaftarSmp = sequelize.define('students', {
    sekolah_id: {
      type: Sequelize.STRING,
      primaryKey: true
    },
    nm_sekolah: {
      type: Sequelize.STRING
    },
    nisn: {
      type: Sequelize.STRING
    },
    npsn: {
      type: Sequelize.STRING
    },
    kd_wil_sekolah: {
      type: Sequelize.STRING
    },
    rombongan_belajar_id: {
      type: Sequelize.UUID
    },
    semester_id: {
      type: Sequelize.BIGINT
    },
    tingkat_pendidikan_id: {
      type: Sequelize.INTEGER
    },
    nm_rb: {
      type: Sequelize.STRING
    },
    ptk_id: {
      type: Sequelize.UUID
    },
    peserta_didik_id: {
      type: Sequelize.UUID
    },
    nama: {
      type: Sequelize.STRING
    },
    jenis_kelamin: {
      type: Sequelize.STRING
    },
    nik: {
      type: Sequelize.STRING
    },
    tempat_lahir: {
      type: Sequelize.STRING
    },
    tanggal_lahir: {
      type: Sequelize.DATE
    },
    agama_id: {
      type: Sequelize.INTEGER
    },
    kewarganegaraan: {
      type: Sequelize.STRING
    },
    kebutuhan_khusus_id: {
      type: Sequelize.INTEGER
    },
    alamat_jalan: {
      type: Sequelize.STRING
    },
    rt: {
      type: Sequelize.DECIMAL
    },
    rw: {
      type: Sequelize.STRING
    },
    nama_dusun: {
      type: Sequelize.STRING
    },
    desa_kelurahan: {
      type: Sequelize.STRING
    },
    kode_wilayah: {
      type: Sequelize.CHAR
    },
    kode_pos: {
      type: Sequelize.CHAR
    },
    lintang: {
      type: Sequelize.DECIMAL
    },
    bujur: {
      type: Sequelize.DECIMAL
    },
    jenis_tinggal_id: {
      type: Sequelize.INTEGER
    },
    alat_transportasi_id: {
      type: Sequelize.INTEGER
    },
    nik_ayah: {
      type: Sequelize.CHAR
    },
    nik_ibu: {
      type: Sequelize.CHAR
    },
    anak_keberapa: {
      type: Sequelize.DECIMAL
    },
    nik_wali: {
      type: Sequelize.CHAR
    },
    nomor_telepon_rumah: {
      type: Sequelize.STRING
    },
    nomor_telepon_seluler: {
      type: Sequelize.STRING
    },
    email: {
      type: Sequelize.STRING
    },
    penerima_KPS: {
      type: Sequelize.DECIMAL
    },
    no_KPS: {
      type: Sequelize.STRING
    },
    layak_PIP: {
      type: Sequelize.DECIMAL
    },
    penerima_KIP: {
      type: Sequelize.DECIMAL
    },
    no_KIP: {
      type: Sequelize.STRING
    },
    nm_KIP: {
      type: Sequelize.STRING
    },
    no_KKS: {
      type: Sequelize.STRING
    },
    reg_akta_lahir: {
      type: Sequelize.STRING
    },
    id_layak_pip: {
      type: Sequelize.DECIMAL
    },
    status_data: {
      type: Sequelize.INTEGER
    },
    nama_ayah: {
      type: Sequelize.STRING
    },
    tahun_lahir_ayah: {
      type: Sequelize.DECIMAL
    },
    jenjang_pendidikan_ayah: {
      type: Sequelize.DECIMAL
    },
    pekerjaan_id_ayah: {
      type: Sequelize.INTEGER
    },
    penghasilan_id_ayah: {
      type: Sequelize.INTEGER
    },
    kebutuhan_khusus_id_ayah: {
      type: Sequelize.INTEGER
    },
    nama_ibu_kandung: {
      type: Sequelize.STRING
    },
    tahun_lahir_ibu: {
      type: Sequelize.DECIMAL
    },
    jenjang_pendidikan_ibu: {
      type: Sequelize.DECIMAL
    },
    penghasilan_id_ibu: {
      type: Sequelize.INTEGER
    },
    kebutuhan_khusus_id_ibu: {
      type: Sequelize.INTEGER
    },
    nama_wali: {
      type: Sequelize.STRING
    },
    tahun_lahir_wali: {
      type: Sequelize.DECIMAL
    },
    jenjang_pendidikan_wali: {
      type: Sequelize.DECIMAL
    },
    pekerjaan_id_wali: {
      type: Sequelize.INTEGER
    },
    penghasilan_id_wali: {
      type: Sequelize.INTEGER
    },
    id_bank: {
      type: Sequelize.CHAR
    },
    nama_kcp: {
      type: Sequelize.STRING
    },
    rekening_bank: {
      type: Sequelize.STRING
    },
    rekening_atas_nama: {
      type: Sequelize.STRING
    },
    create_date: {
      type: Sequelize.DATE
    },
    Soft_delete: {
      type: Sequelize.DECIMAL
    },
    Updater_ID: {
      type: Sequelize.CHAR
    },
    un_1: {
      type: Sequelize.FLOAT
    },
    un_2: {
      type: Sequelize.FLOAT
    },
    un_3: {
      type: Sequelize.FLOAT
    }
  },
  {
    freezeTableName: true,
    timestamps: false   
  });

  module.exports = pendaftarSmp;