const Sequelize = require('sequelize');
const sequelize = require('../connection/percona.js');
const requestSurat = sequelize.define('request_surat', {
    id: {
      type: Sequelize.INTEGER,
      primaryKey: true,
      unique: true,
      autoIncrement: true
    },
    warga_id: {
      type: Sequelize.INTEGER
    },
    surat_id: {
      type: Sequelize.INTEGER
    },
    no_surat: {
      type: Sequelize.STRING
    },
    status: {
      type: Sequelize.INTEGER
    }
  },
  {
    freezeTableName: true,
    underscored : true,
    timestamps: true
  });

var Surat = require('./surat.model.js')

requestSurat.belongsTo(Surat,{
    as: 'surat',
    foreignKey: 'surat_id'
});

module.exports = requestSurat;