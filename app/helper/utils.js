module.exports = {
    calcAge:function(dateString, onDate) {
        var birthday = new Date(dateString);
        var ageDifMs = new Date(onDate).getTime() - birthday.getTime();
        var ageDate = new Date(ageDifMs); // miliseconds from epoch
        return Math.abs(ageDate.getFullYear() - 1970);
    },
    calcPPDB:function(pendaftar, jalur_id, kode_wilayah_sekolah){
        var result = {
          nilaiZona: 0,
          nilaiKhusus: 0,
          nilaiAsalSekolah: 0,
          nilaiPPDB: 0
        };

        // finding nilai_zona
        var nilai_zona = 40;
        var kode_wilayah_siswa = pendaftar.kode_wilayah;
        var kecamatan = kode_wilayah_siswa.slice(0,6); 
        var kabupaten = kode_wilayah_siswa.slice(0,4); 
        console.log("kecamatan", kecamatan);
        console.log("kabupaten", kabupaten);

        console.log("kode_wilayah_sekolah", kode_wilayah_sekolah);
        // 30010204 = 30010209
        if(kode_wilayah_siswa==kode_wilayah_sekolah){
            console.log("sama kelurahan",kode_wilayah_sekolah);
            nilai_zona = 100;
        // 300102 = 300102
        }else if(kecamatan==kode_wilayah_sekolah.slice(0,6)){
            console.log("sama kecamatan",kode_wilayah_sekolah.slice(0,6));
            nilai_zona = 80;
        }else if(kabupaten==kode_wilayah_sekolah.slice(0,4)){
            console.log("sama kabupaten",kode_wilayah_sekolah.slice(0,4));
            nilai_zona = 60;
        }
        console.log("Nilai C:",nilai_zona);

        // finding nilai_asal_sekolah
        var nilai_asal_sekolah = 10;
        if (pendaftar.kota_asal_sekolah=="KOTA GORONTALO"){
          nilai_asal_sekolah = 50;
        }

        switch(parseInt(jalur_id)){
            /**
             * A = Nilai USBN
             * B = Bobot Usia peserta didik
             * C = Bobot Jarak
             * D = Asal Sekolah
             */
            case 1: // zonasi = A + 2B + 3C + D
                var A = parseFloat(pendaftar.un_mat) + parseFloat(pendaftar.un_ipa) + parseFloat(pendaftar.un_bind);
                console.log("Nilai A:",A);
                var B = 100;
                var C = nilai_zona;
                var D = nilai_asal_sekolah;
                result.nilaiZona = nilai_zona;
                result.nilaiPPDB =  A + (2*B) + (3*C) + D;
                break;
            /**
             * A = Nilai USBN
             * B = Bobot Usia peserta didik
             * C = Nilai Prestasi
             * D = asal sekolah
             */
            case 2: // prestasi = 2A + B + 2C + D
                var A = parseFloat(pendaftar.un_mat) + parseFloat(pendaftar.un_ipa) + parseFloat(pendaftar.un_bind);
                var B = 100;
                var C = pendaftar.nil_prestasi;
                var D = nilai_asal_sekolah;
                result.nilaiAsalSekolah = nilai_asal_sekolah;
                result.nilaiPPDB =  (2*A) + B + (2*C) + D;
                break;
            /**
             * A = Nilai USBN
             * B = Bobot Usia peserta didik
             * C = Nilai khusus
             */
            case 3: // khusus = A + B + 2C
              var A = parseFloat(pendaftar.un_mat) + parseFloat(pendaftar.un_ipa) + parseFloat(pendaftar.un_bind);
                var B = 100;
                var C = pendaftar.nil_khusus;
                result.nilaiPPDB =  A + B + (2*C);
                break;
        }
        return result;
    },
    calcPPDBSd:function(pendaftar, jalur_id, kode_wilayah_sekolah){
        var result = {
          nilaiZona: 0,
          nilaiUsia: 0,
          nilaiKhusus: 0,
          nilaiPPDB: 0
        };

        // finding nilai usia
        var onDate = process.env.ON_DATE_SD;
        var tanggalLahir = pendaftar.tanggal_lahir;
        var age = this.parseAge(tanggalLahir,onDate);

        console.log("tanggalLahir", tanggalLahir);
        console.log("age", age);
        // finding nilai usia

        var nilai_usia = 0;
        
        if (age.years >= 5){

          if (age.years == 5){
            if(age.months == 6){
              if(age.days == 0){
                nilai_usia = 70;
              }
            }
          }

          if (age.years >= 5){
            if(age.months >= 6){
              if(age.days > 0){ //  lebih 1 hari
                nilai_usia = 80;
              }
            }
          }

          if (age.years >= 6){
            nilai_usia = 80;
            if(age.months >= 6){
              if(age.days > 0){ //  lebih 1 hari
                nilai_usia = 100;
              }
            }
          }

          if (age.years >= 7){
            nilai_usia = 100;
            if(age.months >= 0){
              if(age.days > 0){ //  lebih 1 hari
                nilai_usia = 120;
              }
            }
          }

        }
        result.nilaiUsia = nilai_usia;
        console.log("final usia bobot :",nilai_usia);

        // finding nilai_zona
        var nilai_zona = 40;
        var kode_wilayah_siswa = pendaftar.kode_wilayah;
        var kecamatan = kode_wilayah_siswa.slice(0,6); 
        var kabupaten = kode_wilayah_siswa.slice(0,4); 
        console.log("kecamatan", kecamatan);
        console.log("kabupaten", kabupaten);
        // 30010204 = 30010209
        if(kode_wilayah_siswa==kode_wilayah_sekolah){
            console.log("sama kelurahan",kode_wilayah_sekolah);
            nilai_zona = 100;
        // 300102 = 300102
        }else if(kecamatan==kode_wilayah_sekolah.slice(0,6)){
            console.log("sama kecamatan",kode_wilayah_sekolah.slice(0,6));
            nilai_zona = 80;
        }else if(kabupaten==kode_wilayah_sekolah.slice(0,4)){
            console.log("sama kabupaten",kode_wilayah_sekolah.slice(0,4));
            nilai_zona = 60;
        }
        console.log("Nilai A:",nilai_usia);
        console.log("Nilai B:",nilai_zona);

        // finding nilai_khusus
        var nilai_khusus = pendaftar.nil_khusus;
        console.log("Nilai C:",nilai_khusus);
        switch(parseInt(jalur_id)){
            /**
             * A = Bobot Usia peserta didik
             * B = Bobot Jarak
             */
            case 1: // zonasi = 2A + B
                var A = nilai_usia;
                var B = nilai_zona;
                result.nilaiZona =  B;
                result.nilaiPPDB =  (2*A) + B;
                console.log("Nilai RESULT:",result);
                break;
            case 2:
                var A = nilai_usia;
                var C = nilai_khusus;
                result.nilaiKhusus  =  C;
                result.nilaiPPDB    =  A + C;
                console.log("Nilai RESULT:",result);
                break;
        }
        return result;
    },
    zeroLeading:function(num,size=4){
        var s = num+"";
        while (s.length < size) s = "0" + s;
        return s;
    },
    parseAge:function(dateString, onDate) {
        var now = new Date(onDate);
      
        var yearNow = now.getYear();
        var monthNow = now.getMonth();
        var dateNow = now.getDate();
      
        var dob = new Date(dateString);
      
        var yearDob = dob.getYear();
        var monthDob = dob.getMonth();
        var dateDob = dob.getDate();
        var age = {};
        var ageString = "";
        var yearString = "";
        var monthString = "";
        var dayString = "";
      
      
        yearAge = yearNow - yearDob;
      
        if (monthNow >= monthDob)
          var monthAge = monthNow - monthDob;
        else {
          yearAge--;
          var monthAge = 12 + monthNow -monthDob;
        }
      
        if (dateNow >= dateDob)
          var dateAge = dateNow - dateDob;
        else {
          monthAge--;
          var dateAge = 31 + dateNow - dateDob;
      
          if (monthAge < 0) {
            monthAge = 11;
            yearAge--;
          }
        }
      
        age = {
            years: yearAge,
            months: monthAge,
            days: dateAge,
            text: ""
            };
      
        if ( age.years > 1 ) yearString = " tahun";
        else yearString = " tahun";
        if ( age.months> 1 ) monthString = " bulan";
        else monthString = " bulan";
        if ( age.days > 1 ) dayString = " hari";
        else dayString = " hari";
      
      
        if ( (age.years > 0) && (age.months > 0) && (age.days > 0) )
          ageString = age.years + yearString + ", " + age.months + monthString + ", and " + age.days + dayString;
        else if ( (age.years == 0) && (age.months == 0) && (age.days > 0) )
          ageString = "Only " + age.days + dayString + " old!";
        else if ( (age.years > 0) && (age.months == 0) && (age.days == 0) )
          ageString = age.years + yearString + " old. Happy Birthday!!";
        else if ( (age.years > 0) && (age.months > 0) && (age.days == 0) )
          ageString = age.years + yearString + " and " + age.months + monthString;
        else if ( (age.years == 0) && (age.months > 0) && (age.days > 0) )
          ageString = age.months + monthString + " and " + age.days + dayString;
        else if ( (age.years > 0) && (age.months == 0) && (age.days > 0) )
          ageString = age.years + yearString + " and " + age.days + dayString;
        else if ( (age.years == 0) && (age.months > 0) && (age.days == 0) )
          ageString = age.months + monthString ;
        else ageString = "belum cukup";
            
        age.text = ageString;
        return age;
      },
      sendMailGmail:function(userEmail, username, password, type){
        const nodemailer = require('nodemailer');
        nodemailer.createTestAccount((err, account) => {
            // create reusable transporter object using the default SMTP transport
            let transporter = nodemailer.createTransport({
                host: 'smtp.gmail.com',
                port: 465,
                secure: true, // true for 465, false for other ports
                auth: {
                    user: process.env.MAIL_USERNAME,//'dika165@gmail.com', // generated ethereal user
                    pass: process.env.MAIL_PASS // generated ethereal password
                }
            });
        
            // setup email data with unicode symbols
            let mailOptions = {
                from: 'PPDB ONLINE KOTA GORONTALO <ppdb@kota-gorontalo.id>', // sender address
                to: userEmail, // list of receivers
                subject: 'Activation Account', // Subject line
                html: "<p>Anda telah mendaftarkan "+type+" : "+username+" untuk melakukan pendaftaran PPDB online Kota Gorontalo, \
                <br>Berikut username dan password anda: </p> \
                <p>Username : "+username+" \
                <br>Password : "+password+" \
                </p> \
                <p>Hormat Kami</p> \
                <br> \
                <br> \
                <br> \
                <p>Panitia PPDB Online Kota Gorontalo</p>"   // html body
            };
        
            // send mail with defined transport object
            transporter.sendMail(mailOptions, (error, info) => {
                if (error) {
                    return console.log(error);
                }
                console.log('Message sent: %s', info.messageId);
                // Preview only available when sending through an Ethereal account
                console.log('Preview URL: %s', nodemailer.getTestMessageUrl(info));
        
                // Message sent: <b658f8ca-6296-ccf4-8306-87d57a0b4321@example.com>
                // Preview URL: https://ethereal.email/message/WaQKMgKddxQDoou...
            });
        });

        
      },
      sendMail:function(userEmail, username, password, type){
        var api_key = process.env.MAIL_API_KEY;//'966f8606c358439832159fea04ffcccf-e44cc7c1-fadf0901';
        var domain = process.env.MAIL_DOMAIN; //'sandbox1503c39758164c319e7aca5e40f50723.mailgun.org';
        var mailgun = require('mailgun-js')({apiKey: api_key, domain: domain});
        
        var data = {
          from: 'PPDB ONLINE KOTA GORONTALO <ppdb@kota-gorontalo.id>',
          to: userEmail,
          subject: 'Activation Account',
          html: "<p>Anda telah mendaftarkan "+type+" : "+username+" untuk melakukan pendaftaran PPDB online Kota Gorontalo, \
                <br>Berikut username dan password anda: </p> \
                <p>Username : "+username+" \
                <br>Password : "+password+" \
                </p> \
                <p>Hormat Kami</p> \
                <br> \
                <br> \
                <br> \
                <p>Panitia PPDB Online Kota Gorontalo</p>"
        };
        
        mailgun.messages().send(data, function (error, body) {
          console.log("email",body);
        });
      },
      sortPilihUrutan: function compare(a,b) {
        if (a.pilih_urut < b.pilih_urut)
          return -1;
        if (a.pilih_urut > b.pilih_urut)
          return 1;
        return 0;
      }
      
}