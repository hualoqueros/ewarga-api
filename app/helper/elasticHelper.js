var client = require('../connection/elasticsearch.js'); 

module.exports = {
    createNewDocument: function(indexName, data){
        return client.index({
                    index: indexName,
                    id: data.id,
                    type: "seleksi",
                    body: data
                });
    },
    createIndexSortir: function(newIndexName){
        return client.indices.create({
                    index:newIndexName,
                    body:{
                        settings:{
                            index:{
                                number_of_shards:5,
                                number_of_replicas:1
                            }
                        },
                        mappings: {
                            seleksi:{
                                properties:{
                                    no_daftar:{
                                        "type":"text"
                                    },
                                    nama:{
                                      "type":"text"
                                    },
                                    nama_orang_tua:{
                                      "type":"text"
                                    },
                                    tanggal_lahir:{
                                      "type":"date"
                                    },
                                    usia:{
                                        "type":"integer"
                                    },
                                    pilih_urut:{
                                        "type":"integer"
                                    },
                                    sekolah_id:{
                                      "type":"integer"
                                    },
                                    sekolah_name:{
                                        "type":"text"
                                    },
                                    jalur:{
                                        "type":"integer"
                                    },
                                    nilai_ppdb:{
                                        "type":"integer"
                                    },
                                    waktu_daftar:{
                                        "type":"date"
                                    }
                                }
                            }
                        }
                    }
                });
    }
}