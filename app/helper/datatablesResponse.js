class DatatablesResponse
{
    constructor() {
        this.draw = 1;
    }

    set recordsTotal(count) {
        this._recordsTotal = count;
    }
    set _recordsFiltered(count) {
        this._recordsFiltered = count;
    }
    set data(data) {
        this._data = data;
    }
    get recordsTotal(){
        return this._recordsTotal;
    }
    get _recordsFiltered(){
        return this.__recordsFiltered;
    }
    get data(){
        return this._data;
    }
}