
const express = require('express');
const bodyParser = require('body-parser');
require('dotenv').config();

var app = express();

// parse requests of content-type - application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }))

// parse requests of content-type - application/json
app.use(bodyParser.json())

const fileUpload = require('express-fileupload')
app.use(fileUpload())
 

/*
const dbConfig = require('./config/db.config.js');
const mongoose = require('mongoose');

mongoose.Promise = global.Promise;

// Connecting to the database
mongoose.connect(dbConfig.url)
.then(() => {
    console.log("Successfully connected to the database");    
}).catch(err => {
    console.log('Could not connect to the database. Exiting now...');
    process.exit();
});
*/

const sequelize = require('./app/connection/percona.js');

sequelize
  .authenticate()
  .then(() => {
    console.log('Percona Connection has been established successfully.');
  })
  .catch(err => {
    console.error('Unable to connect to the database Percona:', err);
  });


app.get('/', function (req, res) {
  res.send('Hello World!');
});

require('./app/routes/surat.route.js')(app);
require('./app/routes/user.route.js')(app);

app.listen(process.env.PORT, function () {
  console.log('Example app listening on port '+process.env.PORT);
});
